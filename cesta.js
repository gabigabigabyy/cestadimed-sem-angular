$(document).ready(function() {
    $('.count').prop('disabled', true);
    $(document).on('click', '.plus', function() {
        $('.count').val(parseInt($('.count').val()) + 1);
    });
    $(document).on('click', '.minus', function() {
        $('.count').val(parseInt($('.count').val()) - 1);
        if ($('.count').val() == 0) {
            $('.count').val(1);
        }
    });
});

$('#passar_mouse').mouseover(function() {
    $('#mostrar').css('display', 'block');
});

$('#passar_mouse').mouseout(function() {
    $('#mostrar').css('display', 'none');
});